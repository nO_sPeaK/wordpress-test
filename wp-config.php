<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '47|{WG+<PT/#Rv@u;KZ3Hv@t+($PdomHR#QQ=138P!r*|g`-:NS,4+BKq LQXQC>' );
define( 'SECURE_AUTH_KEY',  '/V5R[DEw,qFwz0xge1P3>j,<Qn)aMjzFRw&>u4|W,uNE>FlVbVpI-u(5<W1|,/QY' );
define( 'LOGGED_IN_KEY',    'Q`Wo>K r Q!Q&K>+x:FkIwBL9Ai!0=p3t#j,3_Fx>J [j`T[x{}ah`2~Yn1Z-nI8' );
define( 'NONCE_KEY',        'K%6%4,ob&32zL2 `7$eVkP_-QTh)Ytt]p15G<jbovL1fX)p<j}q Q^J|H!wjHqj)' );
define( 'AUTH_SALT',        'Gd6i>|_H~-ajE(y1:;f{XPbbe@;B,h-<jp rtjA/Wo%R(T41gX!SC^`xVU%ff_Ry' );
define( 'SECURE_AUTH_SALT', 'ztR%L>S)P*EJ=~CsfLL/CDt%VAZzUO( P:9dJ<CWmk5+ek/UNA?kc:&thnL6=J=Z' );
define( 'LOGGED_IN_SALT',   'lAEI;fugNJ kTF!x(Uka4eQluIEST2z+pRjDax|>Uji/$a<^Y@PtwupOY]1s2 r-' );
define( 'NONCE_SALT',       'D-}ZZ$8@%V;NIU&I6}<KI6*^Ib{l~H&PgJUlR|}u|;]vFjn&_RdMB={)tE8t!M^2' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
